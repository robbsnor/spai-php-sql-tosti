-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 06 nov 2017 om 10:07
-- Serverversie: 10.1.26-MariaDB
-- PHP-versie: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tostis`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tosti`
--

CREATE TABLE `tosti` (
  `id` tinyint(4) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `broodsoort` varchar(255) NOT NULL,
  `beleg` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `tosti`
--

INSERT INTO `tosti` (`id`, `nickname`, `broodsoort`, `beleg`, `foto`) VALUES
(1, 'Broodkapje', 'Wolvenbollen', 'Mensenvlees', 'https://i.pinimg.com/564x/d1/ac/22/d1ac22659606bbc01010bfc66d18ce21.jpg'),
(3, 'Doornbroodje', 'Spinnenbrood', 'Draad', 'https://www.decoraza.nl/media/catalog/product/cache/8/image/566x566/9df78eab33525d08d6e5fb8d27136e95/d/o/doornroosje-sticker.jpg'),
(4, 'Rapretzel', 'Pretzel', 'Zout', 'https://vignette.wikia.nocookie.net/disney/images/8/82/Rapunzel_pose.png/revision/latest?cb=20160209032533'),
(5, 'De kleine zeemeerbonk', 'Bonkenbrood', 'Vocht', 'https://i.pinimg.com/originals/9e/10/a6/9e10a634f0a3f74305bd184802668444.jpg');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `tosti`
--
ALTER TABLE `tosti`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `tosti`
--
ALTER TABLE `tosti`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
