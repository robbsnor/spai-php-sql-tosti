# spai-php-sql-tosti

### Info
Database naam: tostis

Table naam: tosti

[http://spai.sites.ma-cloud.nl/](http://spai.sites.ma-cloud.nl/)


### Screenshots

## Database

database_verkennen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/database_verkennen.png)

database_structuur
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/database_structuur.png)


## Paginas

tostis_tonen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/tostis_tonen.png)

tosti_toevoegen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/tosti_toevoegen.png)

tosti_aanpassen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/tosti_aanpassen.png)

tosti_verwijderen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/tosti_verwijderen.png)

tosti_tonen
![](https://raw.githubusercontent.com/Robbsnor/spai-php-sql-tosti/master/screenshots/tosti_tonen.png)
