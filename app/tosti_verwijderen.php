<?php include "includes/header.php" ?>

<?php
$id = $_GET['id'];

$query 	= "SELECT 
			id,
			nickname,
			broodsoort,
			beleg,
			foto
		FROM
			tosti 
		WHERE id = " . intval($id) . ";";
	

$result = mysqli_query($conn, $query) or die (mysqli_error($conn));
$tosti = mysqli_fetch_assoc($result);

?>


<div class="container">
	<div class="row">
		<div class="col-12">

			<h1>Tosti verwijderen</h1>
			<p>Weet u zeker dat u deze tosti wilt verwijderen?</p>

			<table class="table table-responsive">
				<?php foreach ($tosti as $key => $value){ ?>

				<?php if($key == "foto"){ ?>

				<tr>
					<th><?=$key?></th>
					<th><a href="<?=$value?>" target="_blank"><?=$value?></a></td>
			    </tr>

				<?php } else { ?>
			    <tr>
			        <th><?=$key?></th>
			        <td><?=$value?></td>
			    </tr>

				<?php }; }; ?>
			</table>

			<form method="post" action="tosti_delete.php">
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $tosti['id']?>" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-primary" href="tostis_tonen.php" role="button">annuleren</a>
					<input type="submit" value="verwijder" class="btn btn-danger">
				</div>
			</form>

		</div>
	</div>
</div>

<?php include "includes/footer.php" ?>