<?php include "includes/header.php" ?>

<?php

$query = "SELECT id, nickname FROM tosti";

$result = mysqli_query($conn, $query)
or die (mysqli_error($conn));

?>

 <div class="container">
 	<div class="row">
 		<div class="col-12">

			<h1>Selecteer tosti</h1>

			<form method="get" action="tostis_tonen.php">
				<select name="id">
					<?php while ($row = mysqli_fetch_assoc($result)){ ?>
					<option value="<?php echo $row['id']; ?>"><?php echo $row['nickname']; ?></option>
					<?php }; ?>
				</select>
				<input type="submit" value="verzenden">
			</form>

 		</div>
 	</div>
 </div>

<?php include "includes/footer.php" ?>