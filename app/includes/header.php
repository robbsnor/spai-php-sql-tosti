<?php include "includes/connect.php" ?>

<!DOCTYPE html>
<html>

<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#ac6d68">

	<title>Tosti's</title>

	<link href="https://fonts.googleapis.com/css?family=Just+Another+Hand" rel="stylesheet">

	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="css/main.min.css">

</head>
<body>

	<div id="hero" class="cover">
		<h1 class="fittext">Tosti's</h1>
	</div>
	