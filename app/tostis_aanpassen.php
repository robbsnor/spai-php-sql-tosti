<?php include "includes/header.php" ?>

<?php
$id = $_GET['id'];

$query 	= "SELECT 
			 id,
			 nickname,
			 broodsoort,
			 beleg,
			 foto
		FROM
			tosti 
		WHERE id = " . intval($id) . ";";
	
$result = mysqli_query($conn, $query) or die (mysqli_error($conn));
$tosti = mysqli_fetch_assoc($result);

?>

<div class="container">
	<div class="row">
		<div class="col-12">

			<h1>Tosti aanpassen</h1>

			<form method="post" action="tosti_update.php">
				<div class="form-group">
					<input type="hidden" name="id" placeholder="id" value="<?php echo $tosti['id']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Nickname</label>
					<input type="text" name="nickname" placeholder="nickname" value="<?php echo $tosti['nickname']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Broodsoort</label>
					<input type="text" name="broodsoort" placeholder="broodsoort" value="<?php echo $tosti['broodsoort']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Beleg</label>
					<input type="text" name="beleg" placeholder="beleg" value="<?php echo $tosti['beleg']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Foto</label>
					<input type="text" name="foto" placeholder="foto" value="<?php echo $tosti['foto']?>" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="tostis_tonen.php" role="button">annuleren</a>
					<input type="submit" value="opslaan" class="btn btn-success">
				</div>
			</form>

		</div>
	</div>
</div>


<?php include "includes/footer.php" ?>