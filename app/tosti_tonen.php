<?php include "includes/header.php" ?>

<?php

$query = "SELECT * FROM tosti;";

$result = mysqli_query($conn, $query)
or die (mysqli_error($conn));

?>

<div class="container">
	<div class="row">
		<div class="col-12">

			<?php if (mysqli_num_rows($result) > 0): ?>
			 
			<table class="table table-responsive">
				<tr>
 					<th>ID</th>
 					<th>nickname</th>
 					<th>broodsoort</th>
 					<th>beleg</th>
 					<th>pic</th>
			        <th></th>
			        <th></th>
				</tr>
				<?php while ($row = mysqli_fetch_assoc($result)): ?>
				<tr>
					<th> <?php echo $row['id']; ?> </th>
					<td> <?php echo $row['nickname']; ?> </td>
				    <td> <?php echo $row['broodsoort']; ?> </td>
				    <td> <?php echo $row['beleg']; ?> </td>
				    <td> <img src="<?php echo $row['foto']; ?>" alt="" width="auto" height="20px"> </td>
				    <td><a href="tostis_aanpassen.php?id=<?php echo $row['id']; ?>">bewerken</a>
			    	<td><a href="tosti_verwijderen.php?id=<?php echo $row['id']; ?>">verwijderen</a>
				</tr>
			<?php endwhile; ?>
			</table>
			 
			<?php else: ?>
			<p class="warning">Geen tosti's gevonden...</p>
			<?php endif; ?>

		</div>
	</div>
</div>


<?php include "includes/footer.php" ?>