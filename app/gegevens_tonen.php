<?php include "includes/header.php" ?>

<?php

$query = "SELECT * FROM tosti";

$result = mysqli_query($conn, $query)
or die (mysqli_error($conn));

?>

 
 <div class="container">
 	<div class="row">
 		<div class="col-12">
			<!-- check of er rows in de tabel zijn ingevoerd -->
			<?php if (mysqli_num_rows($result) > 0){ ?>
			
 			<table class="table table-responsive">
 				<thead>
 					<th>ID</th>
 					<th>nickname</th>
 					<th>broodsoort</th>
 					<th>beleg</th>
 					<th>foto</th>
 				</thead>

 				<tbody>
 					<?php while ($row = mysqli_fetch_assoc($result)){ ?>
 						<tr>
 							<th> <?php echo $row['id']; ?> </th>
 							<td> <?php echo $row['nickname']; ?> </td>
 						    <td> <?php echo $row['broodsoort']; ?> </td>
 						    <td> <?php echo $row['beleg']; ?> </td>
 						    <td> <img src="<?php echo $row['foto']; ?>" alt="" width="auto" height="20px"> </td>
 						</tr>
 					 
 					<?php }; ?>
 				</tbody>

 			</table>
 			 
 			<?php } else { ?>
 				<p class="warning">Geen tosti's gevonden...</p>
 			<?php }; ?>

 		</div>
 	</div>
 </div>

<?php include "includes/footer.php" ?>