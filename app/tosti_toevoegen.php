<?php include "includes/header.php" ?>

 <div class="container">
 	<div class="row">
 		<div class="col-12">

			<h1>Tosti's toevoegen</h1>

			<form method="post" action="tosti_insert.php">
				<div class="form-group">
					<label>Nickname</label>
					<input type="text" name="nickname" placeholder="nickname" class="form-control">
				</div>
				<div class="form-group">
					<label>Broodsoort</label>
					<input type="text" name="broodsoort" placeholder="broodsoort" class="form-control">
				</div>
				<div class="form-group">
					<label>Beleg</label>
					<input type="text" name="beleg" placeholder="beleg" class="form-control">
				</div>
				<div class="form-group">
					<label>Foto</label>
					<input type="text" name="foto" placeholder="foto image url" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="tostis_tonen.php" role="button">annuleren</a>
					<input type="submit" value="toevoegen" class="btn btn-success">
				</div>
			</form>

 		</div>
 	</div>
 </div>

<?php include "includes/footer.php" ?>